<?php

namespace Movement\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Movement\Models\DocumentMovement;

class DocumentMovementController extends Controller
{
    const ALLOWED_ACTION_TYPE = [
        DocumentMovement::ACKNOWLEDGE,
        DocumentMovement::FORWARD,
        DocumentMovement::BACK
    ];

    public function store(Request $request, $tableName, $docuemntId)
    {
        $baseTableConnName = $request->basetableconnectionname ?? null;
        $singularTableName = Str::singular($tableName);
        $movementObject = $this->getMovementObj($singularTableName);
        $foreignKeyColumnName = $singularTableName . "_id";
        $document = DB::connection($baseTableConnName)->table($tableName)->find($docuemntId);
        $documentMovementTrackingId = $document->movement_tracking_id
            ? $document->movement_tracking_id + 1
            : 1;

        DB::beginTransaction();
        $action = $request->action;
        if ($action) {
            DB::connection($baseTableConnName)->table($tableName)->where('id', $docuemntId)
                ->update(['movement_tracking_id' => $documentMovementTrackingId]);
            foreach ($action as $act) {
                $movementData = [
                    $foreignKeyColumnName  => $docuemntId,
                    'movement_tracking_id' => $documentMovementTrackingId,
                    'to_office_id'         => $act['to_office_id'] ?? null,
                    'to_office'            => $act['to_office'] ?? null,
                    'to_designation_id'    => $act['to_designation_id'] ?? null,
                    'to_designation'       => $act['to_designation'] ?? null,
                    'to_role_id'           => $act['to_role_id'] ?? null,
                    'to_role'              => $act['to_role'] ?? null,
                    'to_user_identifier'   => $act['to_user_identifier'] ?? null,
                    'action_type'          => $act['action'],
                ];
                if (isset($act['to_designation_type'])) {
                    $movementData =  array_merge($movementData, ['from_designation_type' => auth()->user()->active_entity_type ?? null, 'to_designation_type' => $act['to_designation_type'] ?? null]);
                }
                $movementObject->create($movementData);
            }
        }

        $acknowledge = $request->acknowledge;

        if ($acknowledge) {
            foreach ($acknowledge as $ack) {
                $movementData = [
                    $foreignKeyColumnName => $docuemntId,
                    'movement_tracking_id' => $documentMovementTrackingId,
                    'to_office_id' => $ack['to_office_id'] ?? null,
                    'to_office' => $ack['to_office'] ?? null,
                    'to_designation_id' => $ack['to_designation_id'] ?? null,
                    'to_designation' => $ack['to_designation'] ?? null,
                    'to_role_id' => $ack['to_role_id'] ?? null,
                    'to_role' => $ack['to_role'] ?? null,
                    'to_user_identifier' => $ack['to_user_identifier'] ?? null,
                    'action_type' => DocumentMovement::ACKNOWLEDGE,
                ];
                if (isset($ack['to_designation_type'])) {
                    $movementData =  array_merge($movementData, ['from_designation_type' => auth()->user()->active_entity_type ?? null, 'to_designation_type' => $ack['to_designation_type'] ?? null]);
                }
                $movementObject->create($movementData);
            }
        }
        DB::commit();

        return response()->json([
            'status' => 'ok',
            'data' => []
        ]);
    }

    private function getMovementObj($tableName)
    {
        $movement = new DocumentMovement();
        $movement->setConnection(config('movements.database.connection'));
        $movement->setTable($tableName . "_movements");
        return $movement;
    }

    public function ValidateMovementRequest($request)
    {
        // return Validator::make(
        //     [
        //         'to_office_id' => $action['to_office_id'],
        //         'to_role_id' => $action['to_role_id'],
        //         'action' => $action['action'],
        //     ],
        //     [
        //         'to_office_id' => 'numeric',
        //         'to_role_id' => 'numeric',
        //         'action' => 'required|in:' . implode(',', self::ALLOWED_ACTION_TYPE)
        //     ]
        // );
    }
}
