<?php
namespace Movement\Views\Components;

use Illuminate\View\Component;
use Pondit\BAF\Masterdata\Models\Appointment;
use Pondit\BAF\Masterdata\Models\ConcernGroup;
use Pondit\BAF\Masterdata\Models\Office;

class MovementComponent extends Component
{
    public $documentId, $documentURL, $documentData;
    public function __construct($documentId = null, $documentURL = null, $documentData = null)
    {
        $this->documentId   = $documentId;
        $this->documentURL  = $documentURL;
        $this->documentData = $documentData;
    }

    public function render()
    {
        $user            = auth()->user();
        $offices         = Office::withoutGlobalScope('ownOfficeScope')->get();
        $appointments    = Appointment::pluck('name', 'id')->toArray();
        $to_appointment  = Appointment::where('is_concern_group_leader', 0)->get();
        $to_concerngroup = ConcernGroup::all();
        return view('movements::components.movement-component', compact('offices', 'appointments', 'to_appointment', 'to_concerngroup'));
    }
}