<?php

namespace Movement\Traits;

use Illuminate\Support\Str;
use Movement\Models\DocumentMovement;
use Illuminate\Database\Eloquent\Relations\HasMany;

trait Moveable
{

    public function movements()
    {
        $tableName                 = Str::singular($this->table);
        return new HasMany($this->getMovementObj()->newQuery(), $this, $tableName . '_id', 'id');
    }

    public function currentMovements()
    {
        return new HasMany($this->getMovementObj()->newQuery(), $this, 'movement_tracking_id', 'movement_tracking_id');
    }

    protected function getMovementObj()
    {
        $tableName                 = Str::singular($this->table);
        $documentMovementTableName = $tableName . "_movements";
        $documentMovement          = new DocumentMovement();
        $documentMovement->setConnection(config('movements.database.connection'));
        $documentMovement->setTable($documentMovementTableName);
        return $documentMovement;
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $user = auth()->user();

            $model->initiation_office_id       = $user->office_id ?? null;
            $model->initiation_office          = $user->offices ?? null;
            $model->initiation_role_id         = $user->active_role_id ?? null;
            $model->initiation_designation_id  = $user->active_entity_id ?? null;
            $model->initiation_designation     = $user->active_entity_name ?? null;
            $model->initiation_user_identifier = $user->service_number ?? null;
        });
    }

    // $filterBy = [
    //     'action_type' => 'ack',
    //     'to_office_id' => '1',
    //     'to_designation_id' => '1',
    //     'to_role_id' => '1',
    //     'to_user_identifier' => '1',
    // ];

    // return $yourRecord->whereHas('movements', function ($query) use ($filterBy)
    //                 {

    //                 $query = isset($filterBy['action_type']) && !is_null($filterBy['action_type']) 
    //                         ? $query : $query->where('action_type', $filterBy['action_type']);

    //                 $query = isset($filterBy['to_office_id']) && !is_null($filterBy['to_office_id']) 
    //                         ? $query : $query->where('to_office_id', $filterBy['to_office_id']);                       

    //                 $query = isset($filterBy['to_role_id']) && !is_null($filterBy['to_role_id']) 
    //                         ? $query : $query->where('to_role_id', $filterBy['to_role_id']);

    //                 $query = isset($filterBy['to_designation_id']) && !is_null($filterBy['to_designation_id']) 
    //                         ? $query : $query->where('to_designation_id', $filterBy['to_designation_id']);

    //                 $query = isset($filterBy['to_user_identifier']) && !is_null($filterBy['to_user_identifier']) 
    //                         ? $query : $query->where('to_user_identifier', $filterBy['to_user_identifier']);                      

    //             })->with('movements')->get();

    //Todo: draftDocuments(), actionDocuments(), acknowledgedDocuments(), forwardedDocuments()
}

// Ref: https://stackoverflow.com/questions/39448066/laravel-eloquent-dynamically-defined-relationship
