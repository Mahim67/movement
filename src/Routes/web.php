<?php

use Illuminate\Support\Facades\Route;
use Movement\Http\Controllers\DocumentMovementController;

Route::group(['middleware' => ['web', 'auth', 'verified'] ], function () {
    Route::post('/{tablename}/{id}/move', [DocumentMovementController::class, 'store']);
    Route::post('/create-demand', [DocumentMovementController::class, 'create']);
});