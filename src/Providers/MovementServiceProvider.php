<?php

namespace Movement\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Movement\Views\Components\MovementComponent;

class MovementServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../Routes/web.php');
        $this->loadViewsFrom(__DIR__ . '/../Resources/views', 'movements');
        $this->loadComponents();
    }

    public function register(): void
    {
        
    }
    public function loadComponents()
    {
        Blade::component('pondit-baf-movement', MovementComponent::class);
    }
}
