<?php

namespace Movement\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema as Schema;
use Illuminate\Support\Str;

class MovementTablesGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'movement:generate-tables {--connection=0}{--table=document}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Movement Ralated Table Generate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $dbConnection      = config('movements.database.connection');
        $basetableConnection         = $this->option('connection')?$this->option('connection'):null;
        $tableName         = $this->option('table');
        $tableNameSingular = Str::singular($tableName);
        $movementTable     = $tableNameSingular.'_movements';
        Schema::connection($basetableConnection)->table($tableName, function($table)  {

            // movement tracking identifier
            $table->unsignedInteger('movement_tracking_id')->nullable();

            // initiation
            $table->unsignedInteger('initiation_office_id')->nullable();
            $table->string('initiation_office')->nullable();

            $table->unsignedInteger('initiation_designation_id')->nullable();
            $table->string('initiation_designation')->nullable();

            $table->unsignedInteger('initiation_role_id')->nullable();
            $table->string('initiation_role')->nullable();

            $table->string('initiation_user_identifier')->nullable();
        });

        Schema::connection($dbConnection)->create($movementTable, function($table) use ($tableNameSingular)  {
            $table->increments('id');
            $table->unsignedBigInteger($tableNameSingular.'_id');

            // movement tracking identifier
            $table->unsignedInteger('movement_tracking_id')->nullable();

            // from
            $table->unsignedInteger('from_office_id')->nullable();
            $table->string('from_office')->nullable();

            $table->unsignedInteger('from_designation_id')->nullable();
            $table->string('from_designation')->nullable();

            $table->unsignedInteger('from_role_id')->nullable();
            $table->string('from_role')->nullable();

            $table->string('from_user_identifier')->nullable();

            // to
            $table->unsignedInteger('to_office_id')->nullable();
            $table->string('to_office')->nullable();

            $table->unsignedInteger('to_designation_id')->nullable();
            $table->string('to_designation')->nullable();

            $table->unsignedInteger('to_role_id')->nullable();
            $table->string('to_role')->nullable();

            $table->string('to_user_identifier')->nullable();

            // action type : fwd, back, ack
            $table->string('action_type')->nullable();
            $table->timestamps();
        });

        $this->info($movementTable.' table created successfully.');
    }
}
