
<a href="#" data-toggle="modal" data-target="#movementModal"><span><i class="fas fa-forward"></i><b> Forward</b></span></a>

<input type="hidden" name="csrf_token" id="csrf_token" value="{{ csrf_token() }}">
<input type="hidden" id="documentId" value="{{ $documentId }}">
<input type="hidden" id="documentURL" value="{{ $documentURL }}">

<!-- Modal -->
<div class="modal fade" id="movementModal" tabindex="-1" role="dialog" aria-labelledby="movementModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="movementModalLabel">Movement</h5>
            <button type="button" class="close text-danger" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
         <form id="mainForm">@csrf 
            {{-- <form action="{{ route('localtenders.movement') }}" method="post">@csrf --}}

                <input type="hidden" name="document_id" id="documentData" value="{{ $documentData->id ?? null }}">

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="office_id" class="col-sm-3 mt-2">Office</label>
                        <div class="col-sm-9">
                            <select name="office_id" id="office_id" onchange="onchange_office(this);"
                                class="form-control select-search">
                                @isset($offices)
                                <option value="">Select Offices</option>
                                    @foreach ($offices as $office)
                                        <option value="{{ $office->id }}">{{ $office->name }}</option>
                                    @endforeach
                                @endisset
                            </select>
                        </div>
                    </div>
                    <div id="movement_details" class="d-none">
                        <div class="form-group row">
                            <label for="" class="col-sm-3"></label>
                            <div class="col-sm-9">
                                <span><input type="radio" name="entity_type" value="Appointment"> Appointment</span><br>
                                <span><input type="radio" name="entity_type" value="Concern Group"> Concern Group</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-3 mt-2 d-flex"><span id="entity_title">Action</span></label>
                            <div class="col-sm-9">
                                <select name="entity_id[]" id="entity_id" class="form-control select-search">
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-3 mt-2">Appointment (Info)</label>
                            <div class="col-sm-9">
                                <select name="ackonowledge_appointment_id[]" id="ackonowledge_appointment_id"
                                class="form-control multiselect-select-all-filtering mb-2" multiple="multiple" data-fouc="">
                                    @isset($appointments)
                                        @foreach ($appointments as $key => $appointment)
                                            <option value="{{ $key }}">{{ $appointment }}</option>
                                        @endforeach
                                    @endisset
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-sm bg-danger" onclick="resetFunc()"
                        data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-sm bg-primary">Send</button>
                </div>
            </form>
        </div>
    </div>
    
</div>
@push('js')
    <script src="{{ asset('vendor/themebazar/ponditlimitless/global_assets/js/plugins/notifications/noty.min.js') }}"></script>
    <script src="{{ asset('vendor/themebazar/ponditlimitless/split/js/flash-message.js') }}"></script>

    <script>
        function onchange_office(item) {
            const documentURL = $("#documentURL").val();
            const documentId  = $("#documentId").val();
            if (item.value != null) {
                $("#movement_details").removeClass("d-none");

                $('input[type=radio][name=entity_type]').change(function() {

                    if (this.value == "Appointment") {
                        $("#entity_title").text('Appointment (Act)');
                        const data = <?php echo json_encode($to_appointment); ?>;
                        let html = '';
                        data.forEach(element => {
                            html += `
                            <option value="${element.id}">${element.name}</option>
                        `;
                        });

                        $("#entity_id").html(`<option value="">Choose Appointment</option>` + html);

                    } else if (this.value == "Concern Group") {
                        $("#entity_title").text('Concern Group (Act)');
                        const data = <?php echo json_encode($to_concerngroup); ?>;
                        let html = '';
                        data.forEach(element => {
                            html += `
                            <option value="${element.id}">${element.name}</option>
                        `;
                        });

                        $("#entity_id").html(`<option value="">Choose Concern Group</option>` + html);
                    }

                });

                $("#mainForm").submit(function(event) {
                    event.preventDefault();
                    let office = $("#office_id").val();

                    var act_designation = [];
                    $('select[name="entity_id[]"] option:selected').each(function() {
                        act_designation.push($(this).val());
                    });
                    var info_designation = [];
                    $('select[name="ackonowledge_appointment_id[]"] option:selected').each(function() {
                        info_designation.push($(this).val());
                    });

                    let act = [];
                    act_designation.forEach((element) => {
                        let actObj = {
                            "to_office_id": office,
                            "to_role_id": null,
                            "to_designation_id": element,
                            "to_user_identifier": null,
                            "action": "fwd"
                        }
                        act.push(actObj)
                    });

                    let ack = [];
                    info_designation.forEach((element) => {
                        let ackObj = {
                            "to_office_id": office,
                            "to_role_id": null,
                            "to_designation_id": element,
                            "to_user_identifier": null
                        }
                        ack.push(ackObj)
                    });
                    let csrf_token = $("#csrf_token").val();

                    const formattingObj = {
                        "action": act,
                        "acknowledge": ack
                    }

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': csrf_token
                        },
                        Accept: 'application/json',
                        type: "POST",
                        url: `/${documentURL}/${documentId}/move`,
                        data: formattingObj,
                        success: function(res) {
                            if (res.status == 'ok') {
                                resetFunc();
                                $("#movementModal").modal('hide');
                                pl_toast("Forwarded Successfully", 'success')
                            }
                        }
                    });
                });
            }
        }

        function resetFunc() {
            $("#office_id").val("").trigger("change");
            $("#movement_details").addClass('d-none');
            $("#mainForm").trigger('reset');
        }
        $('.close').click(function () {
            resetFunc();
        })
    </script>

@endpush
