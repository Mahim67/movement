<?php

namespace Movement\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentMovement extends Model
{
    protected $guarded = [];

    const DRAFT = 'draft';
    const ACKNOWLEDGE = 'ack';
    const FORWARD = 'fwd';
    const BACK = 'back';

    const ACTION_FORWARD = ['last_action_type' => self::FORWARD];
    const ACTION_BACK = ['last_action_type' => self::BACK];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {    
            $user = auth()->user();

            $model->from_office_id       = $user->office_id ?? null;
            $model->from_office          = $user->offices ?? null;
            $model->from_designation_id  = $user->active_entity_id ?? null;
            $model->from_designation     = $user->active_entity_name ?? null;
            $model->from_role_id         = $user->active_role_id ?? null;
            $model->from_role            = null;
            $model->from_user_identifier = $user->service_number ?? null;
        });
    }
}
