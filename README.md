# Document Movement

This package will help you to move any documents from one desk to another desk.

Document Movement allows you to keep a history of movements changes by simply using a trait. Retrieving the movements data is straightforward, making it possible to display it in various ways.

## Relationships

Movement has following relationships that are fully functional and can be eagerly loaded:

- Document has many movements
- Document has many currentMovements

Powered by: pondit.com [![N|Solid](https://pondit.com/ui/frontend/img/logo.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)



## Installation

Create packages directory to project root directory

Copy movement packages directory

Folder structure:
````
yourproject
    -packages
        --movement
````


composer.json

````
 "autoload-dev": {
        "psr-4": {    
            "Movement\\": "packages/movement/src/",
        }
    }
````
config/app.php

````
'providers' => [
    Movement\Providers\MovementServiceProvider::class,
]
````

app/Console/Kernel.php

````
use Movement\Console\Commands\MovementTablesGenerator;


protected $commands = [
    MovementTablesGenerator::class
];
````

Create config/movements.php
````
return [
    /*
    |--------------------------------------------------------------------------
    | Movement Database Configuration
    |--------------------------------------------------------------------------    |
    */
    
    'database' => [
        'connection' => 'your-database-connection-name'  
    ],
];
````


Console command

````
php artisan movement:generate-tables --table=main_table_name
````

This command will generate a tables related to movements of the main table and will update the main table initations info and tracking info.

Use moveable trait in the respective model of the table

````
use Movement\Traits\Moveable;


use Moveable;

````


Update Moveable Trait (replace null to original value)

````
public static function boot()
{
    parent::boot();

    static::creating(function ($model) {               
        $model->initiation_office_id = null;
        $model->initiation_office = null;
        $model->initiation_role_id = null;
        $model->initiation_role = null;
        $model->initiation_designation_id = null;
        $model->initiation_designation = null;
        $model->initiation_user_identifier = null;
    });
}
````

Update Document Movement Model (replace null with original value)

````
    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {               
            $model->from_office_id = null;
            $model->from_office = null;
            $model->from_designation_id = null;
            $model->from_designation = null;
            $model->from_role_id = null;
            $model->from_role = null;
            $model->from_user_identifier = null;
        });
    }
````


Finally, we are ready to move a document !

`your_app_url/main_table_name/your_record_id/move`

Concerns:

For each movement we are tracking the document (office/designation/role/user_identifier). By default these are nullable. If you need exact value, you have to put those inforation (office/designation/role/user_identifier) in session with the format below:

Sample Payload

````
{
        "action": [
               {
                    "to_office_id" : "15",
                    "to_office": "Dte Sup",
                    "to_role_id" : "dd",
                    "to_role" : "Office Admin",
                    "to_designation_id" : 2,
                    "to_designation": "D, Dte Sup",
                    "to_user_identifier" : "8972",
                    "action" : "fwd"                  
                },
                {
                    "to_office_id" : "14",
                    "to_office": "Dte C&E",
                    "to_role_id" : 1,
                    "to_role" : "Directorate",
                    "to_designation_id" : 3,
                    "to_designation": "AD, Dte C&E",
                    "to_user_identifier" : "8971",
                    "action" : "fwd"                  
                }
        ],
        "acknowledge": [
                {
                    "to_office_id" : "15",
                    "to_office": "Dte Sup",
                    "to_role_id" : 1,
                    "to_role" : "Directorate",
                    "to_designation_id" : 3,
                    "to_designation": "DD (Prov), Dte Sup",
                    "to_user_identifier" : "9320"
                },
                {
                    "to_office_id" : "14",
                    "to_office": "Dte C&E",
                    "to_role_id" : 1,
                    "to_role" : "Directorate",
                    "to_designation_id" : 3,
                    "to_designation": "DD , Dte C&E",
                    "to_user_identifier" : "9420"
                }
            ]
}

````



